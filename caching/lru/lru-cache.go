package caching

import (
	"sync"

	linkedlist "codeberg.org/Gusted/algorithms-go/data-structure/linked-list"
)

// A simpe struct that holds the key and value.
// It is used to store the items in the cache.
type lruItem struct {
	value any
	key   string
}

// LRUCache is the public struct.
//
// It holds the capacity, item amount, items and a read/write lock.
// It has a public function for adding items.
// It has a public function for checking if the key exists.
//
// A LRUCache is used to store items in a cache.
// It has a fixed capacity. When the capacity is reached, the last item is removed.
// The items are ordered in the cache, by the order they were added or accessed.
// The items are stored within in a Doubly linked list, and the invidual items
// are stored as well in map[string] for O(1) lookup.
//
// If you access a item via the `Get` function, it will be moved to the front of the cache.
// This behavior doesn't exist for the `Exists` function.
type LRUCache struct {
	rwLock *sync.RWMutex

	itemsList *linkedlist.DoubleLinkedList
	nodeMap   map[string]*linkedlist.Node

	capacity   uint
	itemAmount uint
}

// CreateLRUCache is a public function.
//
// It takes the capacity(int) as an argument.
// It returns a pointer to a LRUCache.
//
// It creates a new LRUCache struct and fills
// it with the necssarry values.
func CreateLRUCache(capacity uint) *LRUCache {
	return &LRUCache{
		capacity:   capacity,
		itemAmount: 0,
		itemsList:  linkedlist.NewDoubleLinkedList(true),
		nodeMap:    make(map[string]*linkedlist.Node),
		rwLock:     &sync.RWMutex{},
	}
}

// removePop is a private function.
//
// It removes the last item in the cache.
func (lru *LRUCache) removePop() {
	// Write lock.
	lru.rwLock.Lock()

	// Remove the latest item from the list.
	// We will first get the last item in the list.
	lastItem := lru.itemsList.Last()

	// We will have to get the cacheKey before it's
	// passed onto Remove() as it will otherwise be
	// removed. to get the cacheKey of the item,
	// we do this by forcing(this is safe) the *lruItem
	// type onto the .Value of the list item.
	// We then get the key of the *lruItem interface.
	cacheKey := lastItem.Value.(lruItem).key

	// We will then remove it from the list itself
	lru.itemsList.Remove(lastItem)

	// Delete the key/value of cacheKey from the map.
	delete(lru.nodeMap, cacheKey)

	// Decrement the item amount.
	lru.itemAmount--

	// Release the write lock.
	lru.rwLock.Unlock()
}

// moveToFront is a private function.
//
// It moves the item to the front of the cache.
// It takes in the index of the item to be moved.
func (lru *LRUCache) moveToFront(listNode *linkedlist.Node) {
	// Write lock.
	lru.rwLock.Lock()

	// We use the moveNodeToFront helper function
	// of the doubly linked list. This will do the hard
	// work of moving the node to the front of the list.
	lru.itemsList.MoveNodeToFront(listNode)

	// Unlock the write lock.
	lru.rwLock.Unlock()
}

// isFull is a private function.
//
// It returns true if the cache is full.
func (lru *LRUCache) isFull() bool {
	lru.rwLock.RLock()
	isFull := lru.itemAmount == lru.capacity
	lru.rwLock.RUnlock()
	return isFull
}

// Add is a public function.
//
// It takes the key(string) and the value(interface{}).
// The function evaluates first if the key already exists.
// If it doesn't exist and the cache is full, we first have to remove the last item.
//
// If the key does exist, and the index of the item is not the first item,
// we have to move the item to the front of the cache.
//
// If the key doesn't exist.
func (lru *LRUCache) Add(key string, value any) {
	// Check if the key already exists.
	listNode, exists := lru.Exist(key)

	// If the key doesn't exist and the cache is full, we have to remove the last item.
	if !exists && lru.isFull() {
		lru.removePop()
	}

	// Check if the key already exists.
	if exists {
		// If the item isn't already the first item,
		// we move it to the front of the cache.
		if listNode != nil {
			lru.moveToFront(listNode)
		}
	} else {
		// Write lock.
		lru.rwLock.Lock()

		// Add a new node to the front of the list.
		// Using the `InsertAtFront` function it will create
		// a new node with the given value and insert it
		// at the front of the list.
		newNode := lru.itemsList.InsertAtFront(lruItem{
			value: value,
			key:   key,
		})

		// Add the newNode to the nodeMap.
		lru.nodeMap[key] = newNode

		// Increment the item amount.
		lru.itemAmount++

		// Unlock the write lock.
		lru.rwLock.Unlock()
	}
}

// Exist is a public function.
//
// It will return a bool and an int.
// The bool will be true if the key exists.
// If the key exist, it will also return the node that is in
// the list, of the given key.
func (lru *LRUCache) Exist(key string) (listNode *linkedlist.Node, exists bool) {
	// Create a read lock.
	lru.rwLock.RLock()

	// Make the unlock read lock in the defer.
	defer lru.rwLock.RUnlock()

	// Check if key is in `lru.nodeMap`
	listNode, exist := lru.nodeMap[key]

	// If it exist, we return listNode, true.
	if exist {
		return listNode, true
	}

	// If it doesn't exist we return nil, false
	return nil, false
}

// Get is a public function.
//
// It will return the value of the key.
// If the key doesn't exist, it will return nil.
//
// It will check if the nodeMap has the key.
// If the key exists, we will move the item to the front of the cache.
// If the key doesn't exist, we will return nil.
func (lru *LRUCache) Get(key string) (value any, exist bool) {
	// Create a read lock.
	lru.rwLock.RLock()

	// Check if key is in `lru.nodeMap`
	listNode, exist := lru.nodeMap[key]

	// Unlock the readLock.
	lru.rwLock.RUnlock()

	// If it doesn't exist we return nil, false
	if !exist {
		return nil, false
	}

	// Move the listNode to the front of the list
	lru.moveToFront(listNode)

	// Cast the lruItem type onto the Value
	lruItem := listNode.Value.(lruItem)

	// Return the value and true.
	return lruItem.value, true
}

// Remove is a public function.
//
// It will return true if it was removed.
// It will return false if the key doesn't exist.
//
// The Remove function will check if the given entry exist in the cache
// and remove the entry from the cache.
func (lru *LRUCache) Remove(key string) bool {
	// Create a read lock.
	lru.rwLock.RLock()

	// Check if key is in `lru.nodeMap`
	listNode, exist := lru.nodeMap[key]

	// Unlock the read lock.
	lru.rwLock.RUnlock()

	// If it doesn't exist we return false as we can't remove
	// a non-existent entry.
	if !exist {
		return false
	}

	// Create a write lock.
	lru.rwLock.Lock()

	// Remove it from the double-linked-list.
	lru.itemsList.Remove(listNode)

	// Remove it from the nodeMap
	delete(lru.nodeMap, key)

	// Decrease the capacity.
	lru.capacity--

	// Unlock write lock.
	lru.rwLock.Unlock()

	// Return true, as we just removed the entry.
	return true
}
