package caching

import (
	"strconv"
	"testing"
)

// This is a test file for the lru-cache implementation.

func TestSimpleTestCase(t *testing.T) {
	t.Parallel()

	// This is a simple test case.
	// Create a LRU-Cache with a capacity of 3.
	cache := CreateLRUCache(3)

	// Add the following elements to the cache.
	cache.Add("1", 1)
	cache.Add("2", 2)
	cache.Add("3", 3)

	// Check if the entries are in the cache.
	if _, ok := cache.Exist("1"); !ok {
		t.Error("Entry 1 is not in the cache.")
	}

	if _, ok := cache.Exist("2"); !ok {
		t.Error("Entry 2 is not in the cache.")
	}

	if _, ok := cache.Exist("3"); !ok {
		t.Error("Entry 3 is not in the cache.")
	}

	// Add a new entry.
	// This should evict the first entry.
	cache.Add("4", 4)

	// Check if the first entry is evicted.
	if _, ok := cache.Exist("1"); ok {
		t.Error("Entry 1 is not evicted.")
	}

	// Check if the new entry is in the cache.
	if _, ok := cache.Exist("4"); !ok {
		t.Error("Entry 4 is not in the cache.")
	}
}

// This is a test case for the LRU-Cache implementation.
// It should test if the entries are moved to the front.
func TestMoveToFrontTestCase(t *testing.T) {
	t.Parallel()

	// Create a cache with a capacity of 3.
	cache := CreateLRUCache(3)

	// Add the following elements to the cache.
	cache.Add("1", 1)
	cache.Add("2", 2)
	cache.Add("3", 3)

	// Check if the entries are in the cache.
	if _, ok := cache.Exist("1"); !ok {
		t.Error("Entry 1 is not in the cache.")
	}

	if _, ok := cache.Exist("2"); !ok {
		t.Error("Entry 2 is not in the cache.")
	}

	if _, ok := cache.Exist("3"); !ok {
		t.Error("Entry 3 is not in the cache.")
	}

	// Get the "1" entry.
	// This should move the entry to the front.
	value, ok := cache.Get("1")
	if !ok {
		t.Error("Entry 1 is not in the cache.")
	}

	if value.(int) != 1 {
		t.Error("Entry 3 has a wrong value.")
	}

	// Add a new entry.
	// So this should mean it will remove the "2" entry.
	// As the "1" entry has been moved to the front.
	cache.Add("4", 4)

	// Check if the "2" entry is evicted.
	if _, ok := cache.Exist("2"); ok {
		t.Error("Entry 2 is not evicted.")
	}

	// Check if the "4" entry is in the cache.
	if _, ok := cache.Exist("4"); !ok {
		t.Error("Entry 4 is not in the cache.")
	}
}

func TestRemove(t *testing.T) {
	t.Parallel()

	// This is to test the Remove functionality.
	// Create a LRU-Cache with a capacity of 3.
	cache := CreateLRUCache(3)

	// Add the following elements to the cache.
	cache.Add("1", 1)
	cache.Add("2", 2)
	cache.Add("3", 3)

	// Check if the entries are in the cache.
	if _, ok := cache.Exist("1"); !ok {
		t.Error("Entry 1 is not in the cache.")
	}

	if _, ok := cache.Exist("2"); !ok {
		t.Error("Entry 2 is not in the cache.")
	}

	if _, ok := cache.Exist("3"); !ok {
		t.Error("Entry 3 is not in the cache.")
	}

	// Remove 3
	if ok := cache.Remove("3"); !ok {
		t.Error("Entry 2 wasn't removed.")
	}
	// Check for existence.
	if _, ok := cache.Exist("3"); ok {
		t.Error("Entry 3 is in the cache.")
	}

	// Add a new entry.
	// This would mean not a lot, as nothing
	// should be ejected.
	cache.Add("4", 4)

	if _, ok := cache.Exist("1"); !ok {
		t.Error("Entry 1 isn't in the cache.")
	}
	if _, ok := cache.Exist("2"); !ok {
		t.Error("Entry 2 isn't in the cache.")
	}
	if _, ok := cache.Exist("4"); !ok {
		t.Error("Entry 4 isn't in the cache.")
	}

	// Try to remove a non-existent key.
	if ok := cache.Remove("10"); ok {
		t.Error("Non-existent entry '10' was removed")
	}
}

// Create a test case to test if their are any race conditions
// This test should be run with the `-race` tag.
func TestRaceConditionTestCase(t *testing.T) {
	t.Parallel()

	// Create a cache with a capacity of 3.
	cache := CreateLRUCache(3)
	// Create a bunch of goroutines which will either
	// Add a entry, or get a entry.

	for i := 0; i < 100; i++ {
		go func(index int) {
			shouldGet := index%2 == 1
			strInt := strconv.Itoa(index)

			if shouldGet {
				cache.Get(strInt)
			} else {
				cache.Add(strInt, index)
			}
		}(i)
	}
}

func BenchmarkSmallAddGetLRUCache(b *testing.B) {
	cache := CreateLRUCache(5)

	for i := 0; i < b.N; i++ {
		strInt := strconv.Itoa(i)
		cache.Add(strInt, i)
		cache.Get(strInt)
	}
}

func BenchmarkBigAddGetLRUCache(b *testing.B) {
	cache := CreateLRUCache(75)

	for i := 0; i < b.N; i++ {
		strInt := strconv.Itoa(i)
		cache.Add(strInt, i)
		cache.Get(strInt)
	}
}

func BenchmarkSmallRandomReadLRUCache(b *testing.B) {
	cache := CreateLRUCache(5)

	for i := 0; i < b.N; i++ {
		// Get a random entry.
		index := i % 100
		strInt := strconv.Itoa(index)
		cache.Get(strInt)
	}
}

func BenchmarkBigRandomReadLRUCache(b *testing.B) {
	cache := CreateLRUCache(75)

	for i := 0; i < b.N; i++ {
		// Get a random entry.
		index := i % 1250
		strInt := strconv.Itoa(index)
		cache.Get(strInt)
	}
}

func BenchmarkSmallAddLRUCache(b *testing.B) {
	cache := CreateLRUCache(5)

	for i := 0; i < b.N; i++ {
		strInt := strconv.Itoa(i)
		cache.Add(strInt, i)
	}
}

func BenchmarkBigAddLRUCache(b *testing.B) {
	cache := CreateLRUCache(75)

	for i := 0; i < b.N; i++ {
		strInt := strconv.Itoa(i)
		cache.Add(strInt, i)
	}
}

func BenchmarkSmallRemoveRUCache(b *testing.B) {
	cache := CreateLRUCache(5)

	for i := 0; i < b.N; i++ {
		strInt := strconv.Itoa(i)
		cache.Add(strInt, i)

		cache.Remove(strInt)
	}
}

func BenchmarkBigRemoveLRUCache(b *testing.B) {
	cache := CreateLRUCache(75)

	for i := 0; i < b.N; i++ {
		strInt := strconv.Itoa(i)
		cache.Add(strInt, i)

		cache.Remove(strInt)
	}
}
