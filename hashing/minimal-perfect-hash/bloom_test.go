package mph

import (
	"sort"
	"testing"
)

func TestBloomFunction(t *testing.T) {
	t.Parallel()

	const amountOfKeys = 7000

	keysSlice := make([]uint64, 0, amountOfKeys)

	for i := 1; i <= amountOfKeys; i++ {
		keysSlice = append(keysSlice, uint64(i))
	}

	newMPH := NewMinimalPerfectHash(keysSlice)

	got := make([]uint64, 0, amountOfKeys)
	for _, v := range keysSlice {
		returnValue := newMPH.Find(v)
		got = append(got, returnValue)
	}

	sort.Slice(got, func(i, j int) bool {
		return got[i] < got[j]
	})

	for i, v := range got {
		if v != uint64(i+1) {
			t.Fatalf("failed: v=%v i+1=%v", v, i+1)
		}
	}
}

func Benchmark_Bloom_Query(b *testing.B) {
	var values []uint64
	for i := 0; i < 1e7; i++ {
		values = append(values, uint64(i))
	}
	h := NewMinimalPerfectHash(values)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		h.Find(uint64(i))
	}
}

func Benchmark_Go_Query(b *testing.B) {
	var values []uint64
	for i := 0; i < 1e7; i++ {
		values = append(values, uint64(i))
	}
	m := make(map[uint64]struct{})
	for _, v := range values {
		m[v] = struct{}{}
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = m[uint64(i)]
	}
}
