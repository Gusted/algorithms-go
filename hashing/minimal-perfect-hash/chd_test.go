package mph

import (
	"encoding/binary"
	"testing"

	"codeberg.org/Gusted/algorithms-go/internal/testutil"
)

func Benchmark_CHD_Construction(b *testing.B) {
	// Make around 5k keys.
	keys := make([]string, 5_000)
	buffer := make([]byte, 4)
	for i := uint32(0); i < 5_000; i++ {
		binary.LittleEndian.PutUint32(buffer, i)
		keys[i] = string(buffer)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		Build(keys)
	}
}

func Benchmark_CHD_Query(b *testing.B) {
	// Make around 5k keys.
	keys := make([]string, 5_000)
	buffer := make([]byte, 4)
	for i := uint32(0); i < 5_000; i++ {
		binary.LittleEndian.PutUint32(buffer, i)
		keys[i] = string(buffer)
	}

	phf := Build(keys)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		phf.Get("average_string")
	}
}

func TestPerfectHashFunction_Random(t *testing.T) {
	t.Parallel()

	if testutil.RaceEnabled {
		t.Skip("Skipping test, see https://github.com/spaolacci/murmur3/issues/29")
		return
	}

	// Make around 5k keys.
	keys := make([]string, 5_000)
	buffer := make([]byte, 4)
	for i := uint32(0); i < 5_000; i++ {
		binary.LittleEndian.PutUint32(buffer, i)
		keys[i] = string(buffer)
	}

	// Build the perfect hash.
	phf := Build(keys)

	buffer = make([]byte, 4)

	// Check if the given indexes correspond to the correct key.
	for i := uint32(0); i < 5_000; i++ {
		binary.LittleEndian.PutUint32(buffer, i)
		if i != phf.Get(string(buffer)) {
			t.Fatalf("Idx: %d doesn't have correct return: %d", i, phf.Get(string(buffer)))
		}
	}
}

var uniqueStrings = []string{
	"Hello, world", "FOSS", "Gitea", "gi21t", "gamining", "emacs", "slices",
	"pointers", "bit twidling", "😀", "e ee ", "lolz", "A quite long sentence!",
}

func TestPerfectHashFunction_Static(t *testing.T) {
	t.Parallel()

	if testutil.RaceEnabled {
		t.Skip("Skipping test, see https://github.com/spaolacci/murmur3/issues/29")
		return
	}

	// Build the perfect hash.
	phf := Build(uniqueStrings)

	// Check if the given indexes correspond to the correct key.
	for i := 0; i < len(uniqueStrings); i++ {
		if uint32(i) != phf.Get(uniqueStrings[i]) {
			t.Fatalf("Idx: %d doesn't have correct return: %d", i, phf.Get(uniqueStrings[i]))
		}
	}
}
