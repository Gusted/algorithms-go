package mph

import (
	"math/bits"
)

// See: https://arxiv.org/pdf/1702.03154.pdf

// BBHash is hash function data.
type BBHash struct {
	bits  []*bitVector
	ranks [][]uint64
}

// BBHashGamma is as stated in the paper, the "best theoretical speed" is achieved with 2.
const BBHashGamma = 10

// NewMinimalPerfectHash returns a new BBHash.
// It takes in the pre-hashed values as a parameter.
func NewMinimalPerfectHash(keys []uint64) *BBHash {
	// Or at least make a option to use sync.pool.
	bbHash := &BBHash{}

	// This finally corresponds to the needed amount of bits slices.
	// but will be used as a mutated variable of the current level.
	currentLevel := 0

	// Calculating the size of the bit vector.
	// It should at least be handling `size * gamma` bits.
	size := uint32(BBHashGamma * len(keys))
	// Rounding it up to the next multiple of 64.
	size = (size + 63) &^ 63

	// Initializing the bit vector for bits over all buckets.
	// Every entry in this set is unique.
	//
	// Note: Within the paper this is referenced as the "A" array.
	// But for verbosity reasons we call it allBits.
	allBits := newBitVector(size)

	// Initializing the bit vector for collision bits.
	// Every entry in this set is unique.
	collisionBits := newBitVector(size)

	// Initialize a nextPass slice.
	// Which will be used as a mutated slice of values
	// which should be used for the next loop iteration.
	// it will not actually be used to iterate over the keys.
	// but rather a temporary variable that later will be set
	// to the keys slice.
	var nextPass []uint64

	// Go loop forever.
	// Until we cannot hash any more keys.
	for len(keys) > 0 {
		// First pass: detect any bits that are colliding.
		for _, key := range keys {
			// Calculate the index of the hashed key.
			index := hash(key, currentLevel) % size

			// Check if the index is already a collision.
			if collisionBits.hasBit(index) {
				continue
			}

			// Check if the index was already created by another key.
			if allBits.hasBit(index) {
				// If it was, mark it as a collision.
				collisionBits.set(index)
				continue
			}

			// Otherwise, mark it as created.
			allBits.set(index)
		}

		// Second pass: create the bits where possible for this level.
		cBitVector := newBitVector(size)
		for _, key := range keys {
			// Calculate the index of the hashed key.
			index := hash(key, currentLevel) % size

			// Check if the index has a collision.
			if collisionBits.hasBit(index) {
				// Add the key to the nextPass slice.
				// This will be used for the next iteration.
				nextPass = append(nextPass, key)
				continue
			}
			// Otherwise add it to the current level.
			cBitVector.set(index)
		}

		// Last step: set the current level to the current bit vector.
		// and prepare all variables for the next iteration.

		// Append cBitVector to the bits slice.
		bbHash.bits = append(bbHash.bits, cBitVector)
		// Set the keys slice to the nextPass slice.
		// This will be used for the next iteration.
		keys = nextPass
		// Reset the nextPass slice.
		nextPass = nextPass[:0]

		// Re-calcaulate the size of the bit vector.
		size = uint32(BBHashGamma * len(keys))
		// Rounding it up to the next multiple of 64.
		size = (size + 63) &^ 63

		// Reset the collision bits and all bits bit vectors.
		collisionBits.reset()
		allBits.reset()

		// increment the current level.
		currentLevel++
	}
	// Calculate the ranks for each level.
	bbHash.computeRanks()

	// All done, after this hard work
	// lets return the bbHash, and be done with it.
	return bbHash
}

// Find is the function that returns the original index of the given key.
func (bb *BBHash) Find(key uint64) uint64 {
	// Do a fast xorshift multiply to get a good hash.
	hash := xorshiftMult64(key)
	// Split up the hash into 2 uint32s.
	// hashLow is the lower 32 bits.
	// hashHigh is the higher 32 bits.
	hashLow, hashHigh := uint32(hash), uint32(hash>>32)
	for lvl, bv := range bb.bits {
		// Rotate the higher bits of the hash to the left by lvl
		// and ^ with the lower bits of the hash.
		hashIndex := (hashLow ^ bits.RotateLeft32(hashHigh, lvl)) % bv.Size()

		// Has this current bit been set?
		if !bv.hasBit(hashIndex) {
			continue
		}

		// Calculate the rank of the current bit.
		rank := bb.ranks[lvl][hashIndex/512]

		// Calculate all the ranks up to the current rank.
		for j := (hashIndex / 64) &^ 7; j < hashIndex/64; j++ {
			rank += uint64(bits.OnesCount64(bv.v[j]))
		}

		// Same as .get() but now to add the last bits for the current rank.
		w := bv.v[hashIndex/64]
		rank += uint64(bits.OnesCount64(w << (64 - (hashIndex % 64))))

		return rank + 1
	}

	return 0
}

// computeRanks calculates the ranks for each level.
// The rank will be calculated as by the paper
// by the amounts of ones in the bit vector.
// This is because all bitVectors begin with 0s.
// And to add a index to the bitVector, we need to add 1.
// So we can calculate the rank by the amount of ones in the bit vector.
// A kind of population count ;).
func (bb *BBHash) computeRanks() {
	var pop uint64
	for _, bv := range bb.bits {
		r := make([]uint64, 0, 1+(len(bv.v)/8))

		for i, v := range bv.v {
			if i%8 == 0 {
				r = append(r, pop)
			}
			pop += uint64(bits.OnesCount64(v))
		}
		bb.ranks = append(bb.ranks, r)
	}
}

// magicTValue is the magic t value used in the paper.
// It's the "recommended" value to use.
const magicTValue = 0x2545f4914f6cdd1d

// xorshiftMult64 is a 64-bit xorshift multiply rng.
// Reference: http://vigna.di.unimi.it/ftp/papers/xorshift.pdf.
func xorshiftMult64(x uint64) uint64 {
	x ^= x >> 12 // a
	x ^= x << 25 // b
	x ^= x >> 27 // c
	return x * magicTValue
}

// hash is the generic hash function used in this implementation.
func hash(key uint64, lvl int) uint32 {
	// Do a fast xorshift multiply to get a good hash.
	hash := xorshiftMult64(key)
	// Split up the hash into 2 uint32s.
	// hashLow is the lower 32 bits.
	// hashHigh is the higher 32 bits.
	hashLow, hashHigh := uint32(hash), uint32(hash>>32)
	// Rotate the higher bits of the hash to the left by lvl
	// and ^ with the lower bits of the hash.
	return (hashLow ^ bits.RotateLeft32(hashHigh, lvl))
}

// bitVector is a bit vector, with implemented methods on it.
type bitVector struct {
	v []uint64
}

// newBitVector returns a new bit vector with the given size.
// It will be initialized with all zeros.
// This should only be used for this specific algorithm,
// as size will be divided which is in par with the functions that are
// implemented on the returned type.
func newBitVector(size uint32) *bitVector {
	bv := &bitVector{
		v: make([]uint64, size/64),
	}
	return bv
}

// hasBit returns 0 if the bit is not set, 1 if it is set.
func (bV *bitVector) hasBit(bit uint32) bool {
	// Calculate the shift amount.
	shift := bit % 64

	// Get the bit where it should be.
	bb := bV.v[bit/64]
	// Check if the bit is set at 2^shift bit.
	bb &= (1 << shift)

	// If the bit was set at 2^shift, then bb should
	// be a non-zero value.
	return bb != 0
}

// set sets the bit at the index to 1.
func (bV *bitVector) set(bit uint32) {
	// This is more or less the reverse of hasBit.
	// The "same" calculation is done here.
	// But this time to set the bit.
	// This can be considered clever code,
	// therefore the comments are more verbose.

	// The shift amount is bit % 64.
	// Now set the bit(b[bit/64]) at the shift's bit index to 1.
	// So just to make it clear.
	// If we b[bit/64] = 0110110(binary) (54 in decimal).
	// And shift is by 6. The 6th bit will be set to 1.
	// And result in 1110110(binary) (118 in decimal).
	bV.v[bit/64] |= (1 << (bit % 64))
}

// Size returns the number of bits in this bitvector.
func (bV *bitVector) Size() uint32 {
	return uint32(len(bV.v) * 64)
}

// reset sets all the bits to 0.
func (bV *bitVector) reset() {
	for i := range bV.v {
		bV.v[i] = 0
	}
}
