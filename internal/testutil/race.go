//go:build race

package testutil

// RaceEnabled reports if the race detector is enabled.
const RaceEnabled = true
