# Algorithms-Go

A wide variety of algorithms, implemented in the awesome go language.

You will a variety of algorithms, all are implemented with zero dependencies.
Licensed under the [MIT](./LICENSE) license, written in idiomatic golang with
explanatory comments.

## Table of Content

- [Caching](#caching)
    - [LRU-Cache](#lru-cache)
- [Hashing](#hashing)
    - [Minimal Perfect Hash](#minimal-perfect-hash)
- [Data Structure](#data-structure)
    - [Double Linked Listed](#double-linked-list)
- [Math](#math)
    - [Is power of two](#is-power-of-two)
    - [Primality test](#primality-test)
    - [Gaussian elimination](#gaussian-elimination)

### Caching

[Caching](https://en.wikipedia.org/wiki/Cache_(computing)) is an algorithm, that stores information, which otherwise would be computed by a slower function. 

#### LRU-Cache

The [LRU-Cache](https://en.wikipedia.org/wiki/Cache_replacement_policies#Least_recently_used_(LRU)). Is the Least-Recently-Used cache.  
It has a fixed capacity. The items are ordered by recently-used, which could mean it was recently added or accessed. When the cache is full it will evict  
the latest entry in the cache, which is the least recently used entry.

[See the code](./caching/lru/lru-cache.go).

### Hashing

[Hashing](https://en.wikipedia.org/wiki/Hash_function) is a function that transforms a given input into an integer.
This function is a one-way function, meaning that it's fast to compute
one-way(transforming the input), but the other way around is
much harder to compute.

#### Minimal Perfect Hash

A regular hash function will transform a given input into an integer.
When two different inputs generate the same integer, it will be called
a collision. These collisions will happen with any kind of universal hashing.

But if you know the inputs(now called keys) in advance, it can be more precise
in avoiding collisions. This is called a perfect hash.

But [minimal perfect hash](https://en.wikipedia.org/wiki/Minimal_perfect_hash_function) goes a little step further, with a perfect hash
each key will be mapped to a unique number, this unique number can be of any
range. With minimal perfect hash, it will ensure that the mapped numbers are
1...N.

[See the code (Bloom-variant)](./hashing/minimal-perfect-hash/bloom-construction.go).  
[See the code (CHD-Variant)](./hashing/minimal-perfect-hash/chd-construction.go).

### Data Structure

A [data structure](https://en.wikipedia.org/wiki/Data_structure) is a collection of values, the relationship among them is
defined by a specific data structure.

#### Double Linked List

The [Double linked list](https://en.wikipedia.org/wiki/Doubly_linked_list)(often called doubly linked list) is a [linked data structure](https://en.wikipedia.org/wiki/Linked_data_structure).
The list consists of a linear sequence of nodes. Each node contains a value
and a pointer to the next and previous node in the linear sequence.
Because this list stores the next and previous node in each node it allows for
bi-directional traversing through the list.

[See the code](./data-structure/linked-list/double-linked-list.go).

### Math

[Math](https://en.wikipedia.org/wiki/Mathematics), the underlying foundation of computer engineering. Within software
engineering the math that's used mostly consists of arithmetics, with the
proper disclaimer that this heavily depends on the project, as for an example
working with graphics will also consist of geometry.

#### Is power of two

In computer science knowing if a number is a [power of two](https://en.wikipedia.org/wiki/Power_of_two) can be quite
handful. Numbers that are a power of two can be optimized in most applications,
by taking advantage of bitwise operations which are faster than normal
arithmetic operations. This code is for the "if" part, it will check if a given
number is a power of two.

[See the code](./math/is-power-of-two/is-power-of-two.go).

#### Primality test

[Primality testing](https://en.wikipedia.org/wiki/Primality_testing) can be done via [trial division](https://en.wikipedia.org/wiki/Trial_division), which is a simple but inefficient way of determining if a given integer is a prime. 

[See the code](./math/primality-test/is-prime.go).

#### Gaussian Elimination

[Gaussian elimination](https://en.wikipedia.org/wiki/Gaussian_elimination) is a method for solving a [system of linear equations](https://en.wikipedia.org/wiki/Systems_of_linear_equations) in [linear algebra](https://en.wikipedia.org/wiki/Linear_algebra).

Solving a system of linear equations using row reduction is a computationally efficient method.
Row echelon form will be the end result. Gaussian-Jordan elimination may be used to further deduce the matrix, yielding the precise values for the factors. This is a reduced row echelon form of the resulting matrix. 

[See the code](./math/row-reduction/gaussian-elimination.go).

### Strings

[Strings](https://en.wikipedia.org/wiki/String_(computer_science)) are an important data type within computer science.
Strings are commonly used in software engineering, they have different meaning
depending on the context in which the string is used. While computers and
CPU's don't directly understand strings, they are represented on a low-level as
a array of bytes, these bytes are {de,en}coded via [the UTF-8](https://en.wikipedia.org/wiki/UTF-8) specification.

#### Levenshtein Distance

[The levenshtein distance](https://en.wikipedia.org/wiki/Levenshtein_distance) is a [string metric](https://en.wikipedia.org/wiki/String_metric),
to calculate the edit distance between two strings. The distance are in other
words the amount of edits you have to make to the string, in order to transform
it into the other string. It's based on the principle of 3 costs,
the substition, addition and deletion.

[See the code](./strings/metrics/levenshtein-distance.go).
