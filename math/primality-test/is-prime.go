package isprime

import "math"

// IsPrime will return if the given input is a prime.
func IsPrime(input uint) bool {
	// Calculate the upper-bound.
	upperBound := uint(math.Sqrt(float64(input)))

	// Start with 2 because, because a prime can always be divided by 1.
	for i := uint(2); i <= upperBound; i++ {
		// If we divide input by i, will the remainder be 0?
		// Then the input isn't a prime.
		if input%i == 0 {
			return false
		}
	}
	// If we reach this code, we know that there is no known number
	// That can divide input and have no remainder.
	return true
}
