package isprime

import "testing"

func TestPrime(t *testing.T) {
	if !IsPrime(7) {
		t.Fatal("7 is a prime.")
	}
	if !IsPrime(17) {
		t.Fatal("17 is a prime.")
	}
	if IsPrime(15) {
		t.Fatal("15 is not a prime.")
	}
	if IsPrime(16) {
		t.Fatal("16 is not a prime.")
	}
}
