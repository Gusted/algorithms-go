package rowreduction

import "testing"

func TestSimpleMatrix(t *testing.T) {
	t.Parallel()

	rowReduced := GaussianElimination(
		[][]float64{
			{2, 1, -1, 8},
			{-3, -1, 2, -11},
			{-2, 1, 2, -3},
		})
	// Check all zeros.
	if rowReduced[1][0] != 0 || rowReduced[2][0] != 0 || rowReduced[2][1] != 0 {
		t.Fatal("Lower triangle isn't zeroed.")
	}

	// Check for factor z.
	if rowReduced[2][3]/rowReduced[2][2] != -1 {
		t.Fatal("Factor z should be -1.")
	}

	// Do a partial "back substuting" to ensure result is correct.
	// Deduce factor z from second row.
	rowReduced[1][3] -= rowReduced[1][2] * -1

	// Check factor y.
	if rowReduced[1][3]/rowReduced[1][1] != 3 {
		t.Fatal("Factor y should be 3.")
	}

	// Deduce factor z and y from first row.
	rowReduced[0][3] -= rowReduced[0][2] * -1
	rowReduced[0][3] -= rowReduced[0][1] * 3

	// Check factor x.
	if rowReduced[0][3]/rowReduced[0][0] != 2 {
		t.Fatal("Factor x should be 2.")
	}
}
