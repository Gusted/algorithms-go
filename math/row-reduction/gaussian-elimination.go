// Package rowreduction provides a Guassian Elimination function.
package rowreduction

// GaussianElimination applies Guassian elimination onto the provided matrix.
// The code expects the input to be a augmented matrix(a m*n matrix whereby n=m+1).
//
// Gaussian elimination will be applied trough iteration of the matrix rows,
// each iteration will:
// - Swap current row with row that has the highest pivot(input[x][i],
// whereby i is the iteration idx and x = 0...amount_rows), this can be the same row.
// - Iterate trough all the rows that are below the pivot row and reduce all
// factors that are in those rows by the delta.
//
// The result matrix will be row echelon form.
func GaussianElimination(input [][]float64) [][]float64 {
	// Get the amount of rows that the supplied matrix has.
	rows := len(input)

	// Iterate trough all rows.
	for i := 0; i < rows; i++ {
		// Check if there's a row that has a higher pivot than the current one
		// and swap the current row with that one.
		rowWithHighestPivot := i
		highestPivot := input[i][i]

		// All previous rows shouldn't be touched, so start searching from
		// the current row.
		for j := i + 1; j < rows; j++ {
			// Is pivot of row higher than current known highest pivot?
			if pivot := input[j][i]; pivot > highestPivot {
				// Change the variables accordingly.
				highestPivot = pivot
				rowWithHighestPivot = j
			}
		}

		// If the current row that has the highest pivot of the column is zero,
		// The matrix is singular and cannot be solved.
		if input[rowWithHighestPivot][i] == 0 {
			panic("Singular matrix detected.")
		}

		// If we found another row that has a higher pivot, swap it.
		if rowWithHighestPivot != i {
			input[i], input[rowWithHighestPivot] = input[rowWithHighestPivot], input[i]
		}

		// Do action for all rows below the pivot row.
		for j := i + 1; j < rows; j++ {
			// Calculate the delta for current row.
			// iterative row[pivot's column] / pivot.
			delta := input[j][i] / input[i][i]

			// Iterate trough the factors of iterative row.
			for k := i + 1; k <= rows; k++ {
				// The "real" trickery of Gaussian elimination.
				// iterative row[factor's column] -= current row[factor's column] * delta.
				input[j][k] -= input[i][k] * delta
			}

			// Zero-out a element in the lower part(in a triangular form).
			input[j][i] = 0
		}
	}

	// Finally return the input.
	return input
}
