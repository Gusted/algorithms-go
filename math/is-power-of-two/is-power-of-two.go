package ispoweroftwo

// IsPowerOfTwo is function that will use bitwise operations
// to determine if the given positive number is a power of 2.
func IsPowerOfTwo(inputNumber uint) bool {
	// In binary, a number is represented with zeros and ones.
	// A number that is power of two is characterized in binary
	// that it has one times a one followed by trailing zeros.
	//
	// Ex.
	// 1000 is a power of 2 number.
	// 1010 is not a power of 2 number.
	//
	// In other words, this functions takes in any input and checks
	// if the given number follows this pattern.
	// Instead of going all-around and count the individual bits,
	// a faster way is possible, if the input is a power of 2,
	// we can subtract one and that would be all ones.
	//
	// Ex.
	// 1000 - 1 = 0111
	//
	// This "mask", can be used in the AND bitwise, which requires that on both
	// numbers the bits are 1 in order for the result to also be 1, with a number
	// that is the power of 2, this should result in 0, as it doesn't have any
	// other ones.

	// Calculate the mask.
	mask := inputNumber - 1

	// With a power of 2 number, this should result in 0.
	return inputNumber&mask == 0
}
