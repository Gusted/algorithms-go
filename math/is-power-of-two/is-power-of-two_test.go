package ispoweroftwo

import "testing"

func TestCheckFunction(t *testing.T) {
	t.Parallel()

	// 8 is a power of 2
	if !IsPowerOfTwo(8) {
		t.Fatal("(8): this number returned false, when it's true")
	}
	// 256 is a power of 2
	if !IsPowerOfTwo(256) {
		t.Fatal("(256): this number returned false, when it's true")
	}

	// 9 is not a power of 2
	if IsPowerOfTwo(9) {
		t.Fatal("(9): this number returned true, when it's false")
	}

	// 69 is not a power of 2
	if IsPowerOfTwo(69) {
		t.Fatal("(69): this number returned true, when it's false")
	}
}
