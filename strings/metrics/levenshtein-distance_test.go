package metrics

import "testing"

func TestLevenshteinDistance(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		firstString  string
		secondString string
		output       int
	}{
		{"kitten", "sitting", 3},
		{"table", "table", 0},
		{"ninewords", "", 9},
		{"", "ninewords", 9},
		{"saturday", "sunday", 3},
		{"assembly", "asmgolang", 7},
		{"theLolIsANiceWord", "LolIsNiceWord", 4},
	}
	for _, tc := range testCases {
		got := CalculateLevenshteinDistance(tc.firstString, tc.secondString)
		if got != tc.output {
			t.Fatalf("\nFirst:    %s\nSecond:   %s\nOutput:   %d\nExpected: %d", tc.firstString, tc.secondString, got, tc.output)
		}
	}
}
