package metrics

// CalculateLevenshteinDistance will take in rwo inputs and give you the
// edit distance of the two strings.
//
// This is the Wagner–Fischer implementation.
//
// The computation is done via matrixes which is constructed with the following
// principle.
//
// Each point in the matrix has a corresponding character on the first and
// second string.
// Because the strings start at +1 in both x and y axis, the [0][x] and [x][0]
// cases will always have the value of x. When this isn't the case, the value
// is determined by the minium value of 3 operations.
//
// 1. Deletion, get the value of [y-1][x].
// The value of [y-1][x] always gets a default value of +1, this is used to get
// the correct value for values that are under the "curve", the curve is defined
// by the values that are "actually" used in order to get the final result,
// hence the naming curve as it's mostly a linear line down to the last value
// in the matrix.
//
// 2. Insertion, get the value of [y][x-1].
// The value of [y][x-1] always gets a default value of +1, this is used to get
// the correct value for values that are above the "curve"
//
// 3. Substitution, get the value of [y-1][x-1].
// The value of the previous character of both strings, this can be seen
// as the "base" cost. This is the only cost who don't get the default +1
// added to it's cost. It only get the extra +1 cost when the characters of
// the string doesn't match.
//
// k > s (Substitution)
// e > i (Substitution)
// > g (Insertion)
// Ex. The distance between "kitten" and "sitting" will be 3.
func CalculateLevenshteinDistance(firstString, secondString string) int {
	// Handle some special cases:
	// If they are the same the distance will be 0.
	if firstString == secondString {
		return 0
	}
	// If one of the strings are empty the distance
	// will be the length of the other string.
	if firstString == "" || secondString == "" {
		return max2(len(firstString), len(secondString))
	}

	// Converting it to []runes, so it's easier to iterate over the characaters.
	s1 := []rune(firstString)
	s2 := []rune(secondString)

	// Calculate the length of both slices.
	// Because these will be used for matrix constructions, +1 will be added.
	// To take into account the [0, x] and [x, 0] will be "empty" and not used
	// for the actual strings.
	s1Len := len(s1) + 1
	s2Len := len(s2) + 1

	// Create the result matrix which holds s1Len matrixes each with a length
	// of s2Len, the slices will be created within the loop.
	// We will be using a optimzed creation of 2-dimensional matrixes
	// Allocate the top-level slice.
	resultMatrix := make([][]int, s1Len)
	// Allocate one large slice to hold all the rows.
	rows := make([]int, s2Len*s1Len)

	// Loop over the rows, slicing each row from the front of the remaining pixels slice.
	for i := range resultMatrix {
		resultMatrix[i], rows = rows[:s2Len], rows[s2Len:]
	}

	// Iterate and calculate all values of the matrix.
	for y := 0; y < s1Len; y++ {
		for x := 0; x < s2Len; x++ {
			// If x or y is 0, return the other value.
			// This to account for the [0, x] and [x, 0] cases.
			if x == 0 || y == 0 {
				resultMatrix[y][x] = max2(x, y)
				continue
			}

			// Check if the characters are the same.
			// For the first string we use the y value.
			// For the second string we use the x value.
			// We use -1, because x and y are taking into account for the
			// [0, x] and [x, 0] cases, which doesn't correspond to a character.
			// Therefor y and x always will have +1, which we need to compensate
			// for.
			if s1[y-1] == s2[x-1] {
				// This means that the distance shouldn't be increased.
				// We will use the substitution cost to fill this value.
				resultMatrix[y][x] = resultMatrix[y-1][x-1]
				continue
			}

			// When they are not equal, the value should be filled
			// by the minium value of the deletion,insertion,substitution cost.
			// Onto that value at this point should always increased by 1,
			// as the characters aren't the same.
			deletionCost := resultMatrix[y-1][x]
			insertionCost := resultMatrix[y][x-1]
			substitutionCost := resultMatrix[y-1][x-1]

			// Get the minium of the values and add 1 onto that.
			resultMatrix[y][x] = 1 + min3(deletionCost, insertionCost, substitutionCost)
		}
	}

	// Return the levenshtein distance, which is the value of the last row.
	return resultMatrix[s1Len-1][s2Len-1]
}

// Some little helper functions.

// max2 is a helper function that takes 2 arguments, hence the naming.
// They differ from math.Max in which allowed type they take in,
// this functions accepts int, math.Max takes float64 as argument.
// max2 will return the maximum value of the 2 arguments.
func max2(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// min3 is a helper function that takes 3 argument, hence the naming.
// They differ from math.Min as this accepts 3 arguments and use the int
// type for the arguments. min3 will return the minimum value of the 3
// arguments.
func min3(a, b, c int) int {
	min := a
	if min > b {
		min = b
	}
	if min > c {
		min = c
	}
	return min
}
