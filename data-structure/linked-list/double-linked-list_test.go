package linkedlist

import (
	"testing"
)

// TestImplementation should test if the NewDoubleLinkedList(false) func returns
// with the correct data structure.
func TestNewFunctionality(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)
	// Every newDoubleLinkedList should return a pointer to a DoubleLinkedList struct.
	if newList == nil {
		t.Fatal("NewDoubleLinkedList(false) should return a non-nill pointer")
	}
	// The root node of a new list should not have a nill prev node.
	if newList.root.prev == nil {
		t.Fatal("The root node should have a prev pointer")
	}
	// The root node of a new list should not have a nill next node.
	if newList.root.next == nil {
		t.Fatal("The root node should have a next pointer")
	}
	// A new initailized list should have a length of 0.
	if newList.Size() != 0 {
		t.Fatal("A new list should have a length of 0")
	}

	// Check if First returns nil when the list is empty.
	if newList.First() != nil {
		t.Fatal("First() should return nil when the list is empty")
	}

	// Check if Last returns nil when the list is empty.
	if newList.Last() != nil {
		t.Fatal("Last() should return nil when the list is empty")
	}
}

// TestPushFront should check if the PushFront functionality works.
func TestPushFront(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Push a new node with the value of 8 to the front of the sequence.
	newNode := newList.InsertAtFront(8)
	// The new node should have a value of 8.
	if newNode.Value != 8 {
		t.Fatal("The new node value of 8 should carry the value of 8")
	}

	// The root node's next contains the first node of the sequence,
	// check if this is properly updated.
	if newList.First() != newNode {
		t.Fatal("First() isn't equal to the newNode which was just pushed to the front")
	}

	// Because it's the only node in this list,
	// Last() should return this node as well
	if newList.Last() != newNode {
		t.Fatal("Last() isn't equal to the only node in the list")
	}

	// The size should be 1
	if newList.Size() != 1 {
		t.Fatal("The size of the newlist isn't 1 when it should be")
	}
}

// TestPushBack should check if the PushBack functionality works.
func TestPushBack(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Push a new node with the value of 48 to the back of the sequence.
	newNode := newList.InsertAtLast(48)
	// The new node should have a value of 48.
	if newNode.Value != 48 {
		t.Fatal("The new node value of 8 should carry the value of 48")
	}

	// The root node's prev contains the last node of the sequence,
	// check if this is properly updated.
	if newList.Last() != newNode {
		t.Fatal("newList.root.prev isn't equal to the newNode which was just pushed to the back")
	}

	// Because it's the only node in this list,
	// First() should return this node as well
	if newList.First() != newNode {
		t.Fatal("First() isn't equal to the only node in the list")
	}

	// The size should be 1
	if newList.Size() != 1 {
		t.Fatal("The size of the newlist isn't 1 when it should be")
	}
}

// TestMixedPush tested the functionality of PushBack and PushFront in 1 list.
func TestMixedPush(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Make a sequence of:
	//   69 <-> 9828
	firstNode := newList.InsertAtFront(69)
	lastNode := newList.InsertAtLast(9828)

	// Now check with First() and Last()
	// If these nodes are in the correct order.

	// Check if 9828 is at the back of the sequence
	if newList.Last() != lastNode {
		t.Fatal("Last() isn't equal to the last node in the sequence")
	}

	// Check if 69 is at the back of the sequence
	if newList.First() != firstNode {
		t.Fatal("First() isn't equal to the first node in the sequence")
	}

	// Make a sequence of:
	//   69 <-> 9828 <-> 562
	lastNode = newList.InsertAtLast(562)

	// Check if 562 is at the back of the sequence
	if newList.Last() != lastNode {
		t.Fatal("Last() isn't equal to the last node in the sequence")
	}

	// Make a sequence of:
	//   829 <-> 69 <-> 9828 <-> 562
	firstNode = newList.InsertAtFront(829)

	// Check if 829 is at the back of the sequence
	if newList.First() != firstNode {
		t.Fatal("First() isn't equal to the first node in the sequence")
	}
}

// TestRemove tested the functionality of "Remove(node)" on the list.
func TestRemove(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Make a sequence of:
	//   69 <-> 9828 <-> 8219 <-> 312
	// We're using just pushFront, so the first node to be pushed will
	// eventually become the last node.
	lastNode := newList.InsertAtFront(312)
	beforeLastNode := newList.InsertAtFront(8219)
	afterFirstNode := newList.InsertAtFront(9828)
	firstNode := newList.InsertAtFront(69)

	// We will remove the node before the last node.
	// The node with the value of 8219.
	newList.Remove(beforeLastNode)

	// Check the size of the list
	if newList.Size() != 3 {
		t.Fatal("Size() doesn't retun the correct size after remove()")
	}

	// Check if the First() and Last() is correct.
	if newList.First() != firstNode {
		t.Fatal("First() doesn't equal to the first node")
	}
	if newList.Last() != lastNode {
		t.Fatal("Last() doesn't equal to the last node")
	}

	// Check if the previous node of last node is
	// the after first node.
	if lastNode.prev != afterFirstNode {
		t.Fatal("The previous node of the last node isn't correct")
	}

	// Check if the fields are properly cleaned up
	if beforeLastNode.list != nil || beforeLastNode.prev != nil || beforeLastNode.next != nil {
		t.Fatal("The node hasn't cleaned up the fields")
	}
}

// TestRandomInsert tested the functionality of the "InsertValue{Before,After}Node" function.
func TestRandomInsert(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Make a sequence of:
	//   69 <-> 9828 <-> 8219 <-> 312
	// We're using just pushFront, so the first node to be pushed will
	// eventually become the last node.
	newList.InsertAtFront(312)
	targetNode := newList.InsertAtFront(8219)
	newList.InsertAtFront(9828)
	newList.InsertAtFront(69)

	// Add a new node with the value of 1231 before the TargetNode.
	// New sequence:
	//   69 <-> 9828 <-> 1231 <-> 8219 <-> 312
	newNode := newList.InsertValueBeforeNode(1231, targetNode)

	// newNode cannot be nil
	if newNode == nil {
		t.Fatal("InsertValueBeforeNode() returned nil")
	}

	// Check if the previous node of targetNode is newNode
	if targetNode.prev != newNode {
		t.Fatal("InsertValueBeforeNode() didn't properly update prev field")
	}

	// Add a new node with the value of 33123 after the newNode.
	// New sequence:
	//   69 <-> 9828 <-> 1231 <-> 33123 <-> 8219 <-> 312

	newNode2 := newList.InsertValueAfterNode(33123, newNode)

	// newNode2 cannot be nil
	if newNode2 == nil {
		t.Fatal("InsertValueAfterNode() returned nil")
	}

	// Check if the previous node of newNode2 is newNode
	// and check if the next node is targetNode
	if newNode2.prev != newNode {
		t.Fatal("InsertValueAfterNode() didn't properly update prev field")
	}
	if newNode2.next != targetNode {
		t.Fatal("InsertValueAfterNode() didn't properly update next field")
	}
}

// TestMoving tested the functionality of the "MoveNodeTo{Front,Back}" function.
func TestMoving(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Make a sequence of:
	//   69 <-> 9828 <-> 8219 <-> 312
	// We're using just pushFront, so the first node to be pushed will
	// eventually become the last node.
	newList.InsertAtFront(312)
	targetNode := newList.InsertAtFront(8219)
	newList.InsertAtFront(9828)
	newList.InsertAtFront(69)

	// Move targetNode to the front.
	// New sequence:
	// 8219 <-> 69 <-> 9828 <-> 312
	newList.MoveNodeToFront(targetNode)

	// Check if .First() returns the correct element
	if newList.First() != targetNode {
		t.Fatal("First() doesn't return the correct node")
	}
	// Check if .Last().prev doesn't return targetNode
	if newList.Last().prev == targetNode {
		t.Fatal("Last().prev return the incorrect node")
	}

	// Move targetNode to the back.
	// New sequence:
	// 69 <-> 9828 <-> 312 <-> 8219
	newList.MoveNodeToBack(targetNode)

	// Check if .Last() returns the correct element
	if newList.Last() != targetNode {
		t.Fatal("Last() doesn't return the correct node")
	}
	// Check if .First() doesn't return targetNode
	if newList.First() == targetNode {
		t.Fatal("First() return the incorrect node")
	}
}

// TestReverse will test the `ReverseOrderOfList` functionality.
func TestReverse(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Make a sequence of:
	//   69 <-> 9828 <-> 8219 <-> 312
	// We're using just pushFront, so the first node to be pushed will
	// eventually become the last node.
	lastNode := newList.InsertAtFront(312)
	thirdNode := newList.InsertAtFront(8219)
	secondNode := newList.InsertAtFront(9828)
	firstNode := newList.InsertAtFront(69)

	// Reverse the list:
	//   312 <-> 8219 <-> 9828 <-> 69
	newList.ReverseOrderOfList()

	// Check if .First() and .Last() return the correct nodes.
	if newList.First() != lastNode {
		t.Fatal("First() doesn't return the correct node")
	}
	if newList.Last() != firstNode {
		t.Fatal("Last() doesn't return the correct node")
	}

	// Check if the third and second node has correctly been reversed.
	if newList.First().next != thirdNode {
		t.Fatal("First().next doesn't have the correct node")
	}
	if newList.Last().prev != secondNode {
		t.Fatal("Last().prev doesn't have the correct node")
	}
}

func assertSliceEqual(t *testing.T, first, second []any) {
	t.Helper()

	if len(first) != len(second) {
		t.Fatal("Slice isn't equal")
	}
	for index, value := range first {
		if value != second[index] {
			t.Fatal("Slice isn't equal at index: ", index)
		}
	}
}

// TestToSlice testes the "ToSlice" functionality.
func TestToSlice(t *testing.T) {
	t.Parallel()

	newList := NewDoubleLinkedList(false)

	// Make a sequence of:
	//   69 <-> 9828 <-> 8219 <-> 312
	// We're using just pushFront, so the first node to be pushed will
	// eventually become the last node.
	newList.InsertAtFront(312)
	newList.InsertAtFront(8219)
	newList.InsertAtFront(9828)
	newList.InsertAtFront(69)

	slicedVersion := newList.ToSlice()

	// Assert that the slicedVersion is equal to the slice how it should be.
	assertSliceEqual(t, slicedVersion, []any{69, 9828, 8219, 312})
}
