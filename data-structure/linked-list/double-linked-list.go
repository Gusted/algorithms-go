package linkedlist

import "sync"

// Node is the struct type for the individual nodes
// that will be in the Double linked list.
//
// It holds the value of the node, which can be anything.
// It holds the pointer of the next and previous node.
// It holds the pointer to the list which the node belongs to.
//
// Node doesn't has any current helper function implemented.
type Node struct {
	// Each node has a value, this value can be any type.
	// Which is why we use interface{}.
	// The value is a public field, so it can be accessed.
	Value any

	// Each node has a next and prev pointer.
	// The next pointer points to the next node in the list.
	// The prev pointer points to the previous node in the list.
	//
	// A example: All nodes have a number Value.
	//    8 <-> 152 <-> 9828 <-> 5 <-> 12344122
	//          ^^^     ^^^^     ^
	//          |||      ||      |
	//          |||    This node |
	//	   Previous node     Next node
	//
	// These are for internal usages and are not private fields.
	// The fields uses pointers to ensure that it's not a copy
	// when the fields are set, but a reference to the same object.
	// So if we change the a node in the sequence, all the nodes
	// will "be affected" by the change, because they all point to
	// the same object and not a copy of it.
	next, prev *Node

	// Because traversing/iterating over the list is O(n)
	// and not so performant, therefor we cannot use this method.
	// We need a method of knowing if a certain Node belongs into
	// A list. Therefor we simply store a pointer of the list
	// into the node, so that the implemented functions
	// can check if a given node belongs to a given list
	// this is a simple pointer compare.
	list *DoubleLinkedList
}

var nodePool = &sync.Pool{
	New: func() any {
		return &Node{}
	},
}

// release is a private helper function that will
// clear the fields on node and put it back into the
// nodePool.
func (n *Node) release() {
	// Clear the fields.
	n.list = nil
	n.next = nil
	n.prev = nil
	n.Value = nil

	// Put it into nodePool
	nodePool.Put(n)
}

// DoubleLinkedList is a Double Linked List structure
//
// It's a linear data structure, which means that it has a sequence of nodes.
// With a "Doubly linked list" each node has a pointer to the previous and next node.
//
// Because the Double Linked List hold a reference to the next and previous node,
// The first and last node are immediately accessible.
//
// This is not a thread-safe implementation. Because of the function calls that you make
// it's better to create your own thread-safe implementation(if needed).
// This is just a simple implementation to show how it works and because the common use cases
// dont need a thread-safe implementation of this data structure.
type DoubleLinkedList struct {
	// The root node is a special node, which is not part of the sequence.
	// It holds a reference to the first and last node in the sequence.
	// It has no Value nor any list initialized as field
	root *Node

	// size holds the number of nodes that are in this sequence.
	// we use uint to ensure that the size is always positive.
	// because we cannot have a negative amount of nodes in the sequence.
	//
	// This is a private field, because it's not meant to be changed
	// by a "user" of the DoubleLinkedList.
	size uint

	// shouldUseNodePool is a simple boolean field
	// that indicates if we should use the nodePool to create
	// new nodes and also release them back into nodePool
	shouldUseNodePool bool
}

// NewDoubleLinkedList creates a new DoubleLinkedList.
// It will initialize the struct with the default parameters.
//
// useNodePool is the only argument, which is a indicator if nodePool
// should be used to create new nodes and if they should be released
// into the nodePool.
func NewDoubleLinkedList(useNodePool bool) *DoubleLinkedList {
	// Initialize the root node.
	rootNode := &Node{}
	// Ensure that next and prev are not nill
	// This to avoid any nill checks in the code.
	rootNode.next = rootNode
	rootNode.prev = rootNode

	// Return the new DoubleLinkedList.
	// With the root node initialized
	// and the size set to zero.
	return &DoubleLinkedList{
		root:              rootNode,
		size:              0,
		shouldUseNodePool: useNodePool,
	}
}

// Size is a little helper function to get the size of the sequence.
// It returns the amount of nodes that are in the sequence.
func (d *DoubleLinkedList) Size() uint {
	return d.size
}

// getNewNode is a private helper function which deals with the
// useNodePool bool, if useNodePool is true we should get one from
// the nodePool and return it, otherwise we can create one ourself.
func (d *DoubleLinkedList) getNewNode() *Node {
	if d.shouldUseNodePool {
		return nodePool.Get().(*Node)
	}
	return &Node{}
}

// First is a little helper function to get the first node in the sequence.
// It returns the first node in the sequence, this is not the case when the sequence is empty.
// Then it will return nil.
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	^
//	|
//
// Return this node.
func (d *DoubleLinkedList) First() *Node {
	// Is the sequence empty?
	if d.size == 0 {
		return nil
	}
	// Return the first node in the sequence.
	// Remember, that d.root.{next, prev} are respectively the first and last node in the sequence.
	return d.root.next
}

// Last is a little helper function to get the last node in the sequence.
// It returns the last node in the sequence, this is not the case when the sequence is empty.
// Then it will return nil.
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	                             ^^^^^^^^
//	                                ||
//	                         Return this node
func (d *DoubleLinkedList) Last() *Node {
	// Is the sequence empty?
	if d.size == 0 {
		return nil
	}
	// Return the last node in the sequence.
	// Remember, that d.root.{next, prev} are respectively the first and last node in the sequence.
	return d.root.prev
}

// insertAt is a little helper function it will insert a new node
// at a given Node, it will return the new node that was added.
// It will always add the node after the target node.
//
// A example: All nodes have a number Value.
//
//	  8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	        ^^^
//	        |||
//	    target node
//
//		insert new node with value of 420
//
// New sequence:
//
//	8 <-> 152 <-> 420 <-> 9828 <-> 5 <-> 12344122
//
// insert new node with value of 420 after the target node.
func (d *DoubleLinkedList) insertAt(target, newNode *Node) *Node {
	// Set the new node's next and prev to the target node's next and prev
	// the prev node will be the target node.
	newNode.prev = target
	// the next node will be the target node's next.
	newNode.next = target.next

	// You now have successfully inserted the new node after the target node.
	// But now you will have to fix the target's node and it's next node
	// to know it's "new" position in the sequence.

	// Set the correct prev and next nodes for the before and after nodes of newNode.

	// So that the prev node knows that newNode is the new next node.
	newNode.prev.next = newNode
	// So that the next node know that newNode is the new prev node.
	newNode.next.prev = newNode

	// As this is a new added node, we need to store the pointer of this list
	// in the node's struct
	newNode.list = d

	// Increment the size of the sequence.
	d.size++

	// Return the newest node in the sequence.
	return newNode
}

// InsertAtFront adds a new node to the front of the sequence.
// It returns the new node that was added.
//
// A example: All nodes have a number Value.
//
//	  8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//
//		insert new node with value of 69
//
// New sequence:
//
//	69 <-> 8 <-> 152 <-> 9828 <-> 5 <-> 12344122
func (d *DoubleLinkedList) InsertAtFront(value any) *Node {
	// Add the node at the front of the sequence
	// we will use the handy insertAt helper function to
	// handle the "hard" work for us.
	// We don't pass d.root.next which is equal to the
	// to the first node in the sequence insertAt will always
	// add the new node _after_ the target node
	// which is incorrect in this code, so we passed &d.root as
	// target node.

	// Get the correct newNode that we can use.
	newNode := d.getNewNode()
	// Set the value of newNode to the correct Value.
	newNode.Value = value

	return d.insertAt(d.root, newNode)
}

// InsertAtLast adds a new node to the back of the sequence.
// It returns the new node that was added.
//
// A example: All nodes have a number Value.
//
//	  8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//
//		insert new node with value of 6941
//
// New sequence:
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122 <-> 6941
func (d *DoubleLinkedList) InsertAtLast(value any) *Node {
	// Add the node at the back of the sequence
	// we will use the handy insertAt helper function to
	// handle the "hard" work for us.
	// We will pass the d.root.prev as target node,
	// which is equal to the last node of the sequence.

	// Get the correct newNode that we can use.
	newNode := d.getNewNode()
	// Set the value of newNode to the correct Value.
	newNode.Value = value

	return d.insertAt(d.root.prev, newNode)
}

// Remove removes a node from the list. It won't return a error
// if the node is not from the given list.
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	              ^^^^
//	               ||
//	        Remove this node
//
// New sequence:
//
//	8 <-> 152 <-> 5 <-> 12344122
func (d *DoubleLinkedList) Remove(node *Node) {
	// Check if node is in this list, only then proceed by removing it.
	if node.list == d {
		// We first have to fix the next and the prev nodes of the given node.
		// We have to tell the next node that their prev node will now be the
		// given node's prev.
		node.next.prev = node.prev
		// And we have to tell the prev node that their next node will now be
		// the given node's next.
		node.prev.next = node.next
		// We've now removed this node from the sequence.

		// To avoid any left-overs on the node, we will remove the
		// prev, next, list and Value field.
		node.list = nil
		node.prev = nil
		node.next = nil
		node.Value = nil

		// Ensure that the list's size decrement.
		d.size--

		// If d.shouldUseNodePool is true, we will release
		// the node into the nodePool so it can be re-used again.
		if d.shouldUseNodePool {
			node.release()
		}
	}
}

// InsertValueBeforeNode will insert a given value before a given node.
// This will let the user insert a new node at a given position in the sequence
// It will return the pointer to the node, if the given node is in the list,
// otherwise it will return a nil.
//
// A example: All nodes have a number Value.
//
//	 8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	               ^^^^
//	                ||
//	         Target node
//
//	Insert a new node with the value of 314 before this node
//
// New sequence:
//
//	8 <-> 152 <-> 314 <-> 9828 <-> 5 <-> 12344122
func (d *DoubleLinkedList) InsertValueBeforeNode(value any, targetNode *Node) *Node {
	// Check if the targetNode is in the list.
	if targetNode.list != d {
		// If that isn't the case return nil.
		return nil
	}
	// Ah great, the given node is in the list
	// Let's add a new node into the list, we will let the
	// helper function insertAt do the "hard" work.
	// We just need to pass the previous node of targetNode
	// as targetNode parameter, this is because within the sequence
	// insertAt will always insert the new node after the target node.
	// So by passing the previous node we will cause the effect that
	// the new node is inserted before the targetNode.

	// Get the correct newNode that we can use.
	newNode := d.getNewNode()
	// Set the value of newNode to the correct Value.
	newNode.Value = value

	return d.insertAt(targetNode.prev, newNode)
}

// InsertValueAfterNode will insert a given value before a given node.
// This will let the user insert a new node at a given position in the sequence
// It will return the pointer to the node, if the given node is in the list,
// otherwise it will return a nil.
//
// A example: All nodes have a number Value.
//
//	 8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	               ^^^^
//	                ||
//	         Target node
//
//	Insert a new node with the value of 314 after this node
//
// New sequence:
//
//	8 <-> 152 <-> 9828 <-> 314 <-> 5 <-> 12344122
func (d *DoubleLinkedList) InsertValueAfterNode(value any, targetNode *Node) *Node {
	// Check if the targetNode is in the list.
	if targetNode.list != d {
		// If that isn't the case return nil.
		return nil
	}
	// Ah great, the given node is in the list
	// Let's add a new node into the list, we will let the
	// helper function insertAt do the "hard" work.
	// We just need to pass the targetNode as parameter,
	// this is because within the sequence insertAt will
	// salways insert the new node after the target node.

	// Get the correct newNode that we can use.
	newNode := d.getNewNode()
	// Set the value of newNode to the correct Value.
	newNode.Value = value

	return d.insertAt(targetNode, newNode)
}

// move is a private function, it will move a given node to a new
// node within the list, the requirement is that both nodes, movingNode
// and targetNode is known. It will move the movingNode to the position
// next of targetNode.
//
// The move function is actually a combined Remove and insertValue
// functionality, but just the changing pointers part. Because of that the
// code itself won't have any comments, just the thing it should do.
func (*DoubleLinkedList) move(movingNode, targetNode *Node) {
	// Ensure movingNode != targetNode
	if movingNode == targetNode {
		return
	}
	// Check the Remove function for comments.
	movingNode.prev.next = movingNode.next
	movingNode.next.prev = movingNode.prev

	// Check the insertValue function for comments.
	movingNode.prev = targetNode
	movingNode.next = targetNode.next
	movingNode.prev.next = movingNode
	movingNode.next.prev = movingNode
}

// MoveNodeToFront will move a given node to the front of the list.
// This won't change the size of the list, just re-locate an existing
// node in the list to a the front of the list.
// It will return nothing, not on success or on failure(node has to be in the list)
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	              ^^^^
//	               ||
//	        Target node
//
// New sequence:
//
//	9828 <-> 8 <-> 152 <-> 5 <-> 12344122
func (d *DoubleLinkedList) MoveNodeToFront(targetNode *Node) {
	// Check if the targetNode is in the list.
	if targetNode.list != d {
		// If not, simply bail out
		return
	}
	// Wooho! The targetNode is in the list.
	// Now let's hand off the hard work to the move
	// helper function. Because `move` will place the node next
	// to the targetNode, we have to pass on `d.root`
	// So it will be placed as the first item in the list, instead
	// of next to the first item in the list.
	d.move(targetNode, d.root)
}

// MoveNodeToBack will move a given node to the back of the list.
// This won't change the size of the list, just re-locate an existing
// node in the list to a the back of the list.
// It will return nothing, not on success or on failure(node has to be in the list).
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//	              ^^^^
//	               ||
//	        Target node
//
// New sequence:
//
//	8 <-> 152 <-> 5 <-> 12344122 <-> 9828
func (d *DoubleLinkedList) MoveNodeToBack(targetNode *Node) {
	// Check if the targetNode is in the list.
	if targetNode.list != d {
		// If not, simply bail out
		return
	}
	// Wooho! The targetNode is in the list.
	// Now let's hand off the hard work to the move
	// helper function. Because `move` will place the node next
	// to the targetNode, we simply have to pass on d.root.prev.
	d.move(targetNode, d.root.prev)
}

// ReverseOrderOfList is a common task in linked list structures.
// It will reverse the order of nodes in the list, such that
// the first node in the list will eventually become the last
// node in the list.
// The size of the list won't change, it will simply change the
// order of nodes.
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//
//	            Reverse this list.
//
// New sequence:
//
//	12344122 <-> 5 <-> 9828 <-> 152 <-> 8
func (d *DoubleLinkedList) ReverseOrderOfList() {
	// We will be using a recursive approach to reverse the order of the list.
	// We will be iterating over all nodes in the list, we will perform a
	// little magic trick to move around some data. To be more precise,
	// We will swap the prev and next pointer.
	//
	// Give a sequence:
	//  9828 <-> 5 <-> 32 <-> 12344122
	//
	// The node 9828:
	// Value: 9828
	// Prev: Root node
	// Next: Node with value 5
	// If we move swap the Prev and Next value
	// the new sequence becomes:
	// 5 <-> 32 <-> 12344122 <-> 9828
	//
	// If we do this for all nodes, it will eventually
	// have the reversed order of the list.

	// The current variable will contain the working node
	// It starts at the first node and eventually ends at the last node.
	current := d.root.next

	// We will now loop indefintetly until we hit the root node.
	// This is because we have to stop when we've looped trough all nodes.
	// When we're at the last node, the .next value will be set as current
	// node, this will actually be d.root, so when we know that current is d.root
	// we know that we've just did the swapping on the last node and therefor
	// stop the loop.
	for current != d.root {
		// Swap the current.prev pointer with current.next
		current.prev, current.next = current.next, current.prev
		// We now have to specify the next `current` node.
		// We will set it to current.prev, this is because the list isn't
		// yet reversed entirely yet, so the .prev node still has the
		// "other" direction, current.prev was just set to current.next pointer
		// so in the "original" state of the list, that will be next pointer in the
		// list.
		current = current.prev
	}
	// I might have lied, we also need to perform the swap operation on d.root
	// But because we cannot detect(without a counter or a boolean) if we just
	// did the operation on d.root, we will just do it outside of the loop.
	d.root.prev, d.root.next = d.root.next, d.root.prev
}

// ToSlice can be considered a helper function to convert the current list
// to a slice. The returned slices will only contain the values of the nodes,
// the order of the slice is the same as the list. The first item in the slice is
// the value of the first node in the list.
//
// A example: All nodes have a number Value.
//
//	8 <-> 152 <-> 9828 <-> 5 <-> 12344122
//
//	         Convert to a slice
//
// Returns:
// []interface{}{8, 152, 9828, 5, 12344122}.
func (d *DoubleLinkedList) ToSlice() []any {
	// Create the slice which will eventually be returned.
	// We already know the size of the slice, therefor we can
	// pre-allocate the capacity for it.
	sliceResult := make([]any, 0, d.size)

	// The current variable will contain the working node
	// It starts at the first node and eventually ends at the last node.
	current := d.root.next

	// We will now loop indefintetly until we hit the root node.
	// This is because we have to stop when we've looped trough all nodes.
	// When we're at the last node, the .next value will be set as current
	// node, this will actually be d.root, so when we know that current is d.root
	// we know that we've just did the swapping on the last node and therefor
	// stop the loop.
	for current != d.root {
		// Append the variable of current's value to the slice.
		sliceResult = append(sliceResult, current.Value)
		// Now set the next variable which needs to be iterated over.
		current = current.next
	}
	return sliceResult
}
