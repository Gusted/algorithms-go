// Package lockfree provides a lock-free non-blocking FIFO queue.
// Described in https://doi.org/10.1145/248052.248106
package lockfree

import (
	"sync/atomic"
)

// Queue implements a singly-linked list with a Push and Pop function.
//
// Head always points to a dummy node, so we can assume head is never nil within
// the dequeue code.
// Tails always points to the last or second to last node in the list.
//
// This Queue has some properties. See 3.1 to see the proof
// 1. The linked list is always connected.
// 2. Nodes are only inserted after the last node in the linked list.
// 3. Nodes are only deleted from the beginning of the linked list.
// 4. Head always points to the first node in the linked list.
// 5. Tail always points to a node in the linked list.
type Queue[T any] struct {
	head atomic.Pointer[node[T]]
	tail atomic.Pointer[node[T]]
}

type node[T any] struct {
	value T
	next  atomic.Pointer[node[T]]
}

// New will return an empty queue.
func New[T any]() *Queue[T] {
	// Create the queue struct.
	queue := &Queue[T]{}

	// Create a dummy node and insert it atomically into head and tail.
	dummy := &node[T]{}
	queue.head.Store(dummy)
	queue.tail.Store(dummy)

	return queue
}

// Push a new value onto the queue.
func (queue *Queue[T]) Push(value T) {
	// Create a new node.
	node := &node[T]{value: value}

	// Loop until we successfully pushed the node onto the queue.
	for {
		// Load tail and read next.
		tail := queue.tail.Load()
		next := tail.next.Load()
		// Is tail still consistent?
		if tail == queue.tail.Load() {
			// Is tail still pointing to the last node?
			if next == nil {
				// In that case we can try insert our node at tail.next,
				if tail.next.CompareAndSwap(next, node) {
					// We're now the last node, so lets update the tail to
					// reflect this change.
					queue.tail.CompareAndSwap(tail, node)
					break
				}
				continue
			}

			// Tail is not pointing to the last node, lets be generous and
			// try to fix it.
			queue.tail.CompareAndSwap(tail, next)
		}
	}
}

// Pop will return the next value in the queue, if the queue isn't empty
// otherwise ok will return false.
func (queue *Queue[T]) Pop() (value T, ok bool) {
	var val T

	for {
		// Load head and tail and read head's next.
		head := queue.head.Load()
		tail := queue.tail.Load()
		next := head.next.Load()
		// Is head still consistent?
		if head == queue.head.Load() {
			// Check if head and tail is equal.
			if head == tail {
				// It is possible that tail is not pointing to the last node, so
				// we check if head.next is nil.
				if next == nil {
					// Queue is empty.
					return val, false
				}
				// Lets be generous and try to make tail up to date.
				queue.tail.CompareAndSwap(tail, next)
				continue
			}

			// Read the value before CAS, it's possible that another Pop
			// operation might free the next node.
			val = next.value
			// Try to move head to the next item in the queue.
			if queue.head.CompareAndSwap(head, next) {
				// If the operation succeeded and head was swapped, we can
				// break the loop.
				break
			}
		}
	}

	return val, true
}
