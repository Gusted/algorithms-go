package lockfree

import (
	"sync"
	"testing"
)

func TestSimple(t *testing.T) {
	t.Parallel()

	q := New[string]()
	q.Push("First")
	q.Push("Second")
	if val, ok := q.Pop(); !ok || val != "First" {
		t.Fatalf("Pop() didn't return correct value [ok=%t,val=%q]", ok, val)
	}
	if val, ok := q.Pop(); !ok || val != "Second" {
		t.Fatalf("Pop() didn't return correct value [ok=%t,val=%q]", ok, val)
	}

	q.Push("First")
	if val, ok := q.Pop(); !ok || val != "First" {
		t.Fatalf("Pop() didn't return correct value [ok=%t,val=%q]", ok, val)
	}

	q.Push("First")
	q.Push("Second")

	if val, ok := q.Pop(); !ok || val != "First" {
		t.Fatalf("Pop() didn't return correct value [ok=%t,val=%q]", ok, val)
	}

	q.Push("Third")

	if val, ok := q.Pop(); !ok || val != "Second" {
		t.Fatalf("Pop() didn't return correct value [ok=%t,val=%q]", ok, val)
	}
	if val, ok := q.Pop(); !ok || val != "Third" {
		t.Fatalf("Pop() didn't return correct value [ok=%t,val=%q]", ok, val)
	}
}

func TestRaceconditions(t *testing.T) {
	t.Parallel()

	q := New[int]()
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		for i := 0; i < 100000; i++ {
			q.Push(1)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < 100000; i++ {
			q.Pop()
		}
		wg.Done()
	}()

	wg.Wait()
}

func TestConcurrency(t *testing.T) {
	t.Parallel()

	q := New[int]()
	wg := sync.WaitGroup{}
	wg.Add(2)

	runs := 100000

	// Outvalue will contain the result of 1+2+3+4...+runs
	outValue := 0

	// Make a goroutine that will push items.
	go func() {
		for i := 0; i <= runs; i++ {
			q.Push(i)
		}
		wg.Done()
	}()

	// Make a goroutine that will pop items.
	go func() {
		for i := 0; i < runs; i++ {
			val, ok := q.Pop()
			if ok {
				outValue += val
			}
		}
		wg.Done()
	}()

	// Wait until both operations are finished.
	wg.Wait()
	// Pop items until the queue is empty.
	for {
		val, ok := q.Pop()
		if !ok {
			break
		}
		outValue += val
	}

	// Use n*(n+1)/2 to check if outValue contains every value that got in.
	if outValue != (runs*(runs+1))/2 {
		t.Error("Every valid that got in, didn't go out")
	}
}
