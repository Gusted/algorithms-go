// Package lockfree provides a concurrent blocking livelock-free FIFO queue.
// Described in https://doi.org/10.1145/248052.248106
package twolock

import (
	"sync"
)

// Queue implements a singly-linked list with a Push and Pop function.
//
// Head always points to a dummy node, so we can assume head is never nil within
// the dequeue code.
// Tails always points to the last or second to last node in the list.
//
// This Queue has some properties. See 3.1 to see the proof
// 1. The linked list is always connected.
// 2. Nodes are only inserted after the last node in the linked list.
// 3. Nodes are only deleted from the beginning of the linked list.
// 4. Head always points to the first node in the linked list.
// 5. Tail always points to a node in the linked list.
type Queue[T any] struct {
	head     *node[T]
	tail     *node[T]
	tailLock sync.Mutex
	headLock sync.Mutex
}

type node[T any] struct {
	next  *node[T]
	value T
}

// New will return an empty queue.
func New[T any]() *Queue[T] {
	dummyNode := &node[T]{}
	return &Queue[T]{
		head: dummyNode,
		tail: dummyNode,
	}
}

// Push will insert a new value onto the queue.
func (queue *Queue[T]) Push(data T) {
	// Create node
	node := &node[T]{value: data}

	// Acquire the tail lock.
	queue.tailLock.Lock()

	// Update the current last node to point to the new node.
	queue.tail.next = node
	// Update the new node to be the tail.
	queue.tail = node

	// Release tail lock.
	queue.tailLock.Unlock()
}

// Pop will return the next value in the queue, if the queue isn't empty
// otherwise ok will return false.
func (queue *Queue[T]) Pop() (value T, ok bool) {
	// Accquire head lock.
	queue.headLock.Lock()

	// Read the head node.
	node := queue.head
	// Read the possible new head node.
	newHead := node.next

	// We know that head always points to a dummy node that doesn't "exists" in
	// the queue, that means we have to check the next value of that dummy node
	// in order to determine if the queue is empty or not.
	if newHead == nil {
		// The queue is empty, unlock the head lock.
		queue.headLock.Unlock()
		return value, ok
	}

	// Queue is not empty, so read the value.
	value = newHead.value

	// Update the head node.
	queue.head = newHead

	// Unlock the headlock.
	queue.headLock.Unlock()

	return value, true
}
